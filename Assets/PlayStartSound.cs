﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayStartSound : MonoBehaviour
{
    public void onClick()
    {
        AkSoundEngine.PostEvent("Start", gameObject);
    }
}
