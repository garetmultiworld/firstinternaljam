﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelTrigger : TriggerInterface
{

    public TriggerInterface TriggerToCancel;

    public override void Cancel()
    {}

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        TriggerToCancel.Cancel();
    }

}
