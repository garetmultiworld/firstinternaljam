﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChanceGroupTriggerItem
{

    public TriggerInterface TriggerToFire;
    [Range(0f, 100f)]
    public float Chances = 100f;

    public void Trigger()
    {
        TriggerToFire.Trigger();
    }

}
