﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnStartTrigger : MonoBehaviour
{

    public TriggerInterface Trigger;

    void Start()
    {
        if (Trigger != null)
        {
            Trigger.Trigger();
        }
    }

}
