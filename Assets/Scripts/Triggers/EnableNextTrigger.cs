﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableNextTrigger : TriggerInterface
{

    public GameObject ObjectToDisable;
    public GameObject ObjectToEnable;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        ObjectToEnable.SetActive(true);
        ObjectToDisable.SetActive(false);
    }
}
