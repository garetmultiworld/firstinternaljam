﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnableInputTrigger : TriggerInterface
{
    public GameObject Parent;
    public string InputName;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        InputTrigger[] Triggers;
        if (Parent != null)
        {
            Triggers = Parent.GetComponents<InputTrigger>();
        }
        else
        {
            Triggers = this.gameObject.GetComponents<InputTrigger>();
        }
        foreach (InputTrigger trigger in Triggers)
        {
            if (InputName.Equals(trigger.InputName))
            {
                trigger.SetReady(true);
            }
        }
    }
}
