﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupTrigger : TriggerInterface
{

    public TriggerInterface[] Triggers;

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach (TriggerInterface trigger in Triggers)
        {
            if (trigger != null)
            {
                trigger.Trigger();
            }
        }
    }

    public override void Cancel()
    {
        foreach (TriggerInterface trigger in Triggers)
        {
            if (trigger != null)
            {
                trigger.Cancel();
            }
        }
    }

}
