﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorTrigger : TriggerInterface
{

    public Door[] doors;

    public override void Cancel()
    {}

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach (Door door in doors)
        {
            door.Open();
        }
    }

}
