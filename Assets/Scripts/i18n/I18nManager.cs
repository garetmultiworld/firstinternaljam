﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum I18nLanguages
{
    English,
    Spanish
}

public class I18nManager : MonoBehaviour
{
    #region Static Instance
    private static I18nManager instance;
    public static I18nManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<I18nManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned I18nManager", typeof(I18nManager)).GetComponent<I18nManager>();
                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    #endregion

    public I18nLanguages CurrentLanguage;

    public I18nFolder[] Folders;

    protected I18nFolder FindFolderByName(string name)
    {
        I18nFolder currentFolder = null;
        foreach (I18nFolder folder in Folders)
        {
            if (folder.Name.Equals(name))
            {
                currentFolder = folder;
                break;
            }
        }
        return currentFolder;
    }

    public string GetFolderItem(string folder,string item)
    {
        string result = "I18n Item not defined yet";
        string[] parts = { folder, item };
        string tmpResult;
        int index = 0;
        I18nFolder currentFolder = FindFolderByName(parts[index]);
        if (currentFolder != null)
        {
            index++;
            while (index < parts.Length)
            {
                tmpResult = currentFolder.FindItemByName(parts[index], CurrentLanguage);
                if (!tmpResult.Equals(""))
                {
                    result = tmpResult;
                    break;
                }
                index++;
            }
        }
        return result;
    }

    public string GetItem(string Name)
    {
        string result = "I18n Item not defined yet";
        string[] parts = Name.Split('/');
        string tmpResult;
        int index = 0;
        I18nFolder currentFolder = FindFolderByName(parts[index]);
        if (currentFolder != null)
        {
            index++;
            while (index < parts.Length)
            {
                tmpResult = currentFolder.FindItemByName(parts[index], CurrentLanguage);
                if (!tmpResult.Equals(""))
                {
                    result = tmpResult;
                    break;
                }
                index++;
            } 
        }
        return result;
    }
}
