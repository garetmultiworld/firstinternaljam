﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class I18nFolder
{

    public string Name;
    public I18nItem []Items;

    public string FindItemByName(string name, I18nLanguages CurrentLanguage)
    {
        string result="";
        foreach (I18nItem item in Items)
        {
            if (item.Name.Equals(name))
            {
                result = item.GetI18n(name, CurrentLanguage);
            }
        }
        return result;
    }

}
