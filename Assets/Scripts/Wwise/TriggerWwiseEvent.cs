﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerWwiseEvent : TriggerInterface
{

    public AK.Wwise.Event WwiseEvent;
    public GameObject TheObject;

    public override void Cancel()
    {
        WwiseEvent.Stop(TheObject);
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        WwiseEvent.Post(TheObject);
    }
}
