﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class CountDownTrigger : TriggerInterface
{

    public float TotalTime;
    public TriggerInterface OnFinish;
    public Text MinuteText;
    public Text SecondText;
    public Text MilisecondText;
    public bool DisplayInSeconds;

    protected bool IsPlaying = false;

    public override void Cancel()
    {
        IsPlaying = false;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        IsPlaying = true;
        SetLabel(TotalTime);
        StartCoroutine(CountDown());
    }

    private void SetLabelSeconds(float time)
    {
        if (SecondText != null)
        {
            int seconds = (int)Math.Floor(time);
            if (seconds < 10)
            {
                SecondText.text = "0" + seconds.ToString();
            }
            else
            {
                SecondText.text = seconds.ToString();
            }
        }
    }

    private void SetLabel(float time)
    {
        if (DisplayInSeconds)
        {
            SetLabelSeconds(time);
            return;
        }
        int minutes = (int)Math.Floor(time / 60);
        if (MinuteText != null)
        {
            if (minutes < 10)
            {
                MinuteText.text = "0" + minutes.ToString();
            }
            else
            {
                MinuteText.text = minutes.ToString();
            }
        }
        if (SecondText != null)
        {
            int seconds = (int)Math.Floor(time - (minutes * 60));
            if (seconds < 10)
            {
                SecondText.text = "0" + seconds.ToString();
            }
            else
            {
                SecondText.text = seconds.ToString();
            }
        }
        if (MilisecondText != null)
        {
            int miliseconds = ((int)Math.Floor(time * 100)) - (((int)Math.Floor(time)) * 100);
            if (miliseconds < 10)
            {
                MilisecondText.text = "0" + miliseconds.ToString();
            }
            else
            {
                MilisecondText.text = miliseconds.ToString();
            }
        }
    }

    private IEnumerator CountDown()
    {
        float time = 0;
        for (time = TotalTime; time > 0 && IsPlaying; time-=Time.deltaTime)
        {
            SetLabel(time);
            yield return null;
        }
        if (IsPlaying)
        {
            if (time < 0)
            {
                time = 0;
            }
            SetLabel(time);
            if (OnFinish != null)
            {
                OnFinish.Trigger();
            }
        }
    }

}
