﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTrigger : TriggerInterface
{

    public GameObject Prefab;
    public GameObject Parent;
    public RectBounds Bounds;
    public RectBounds.AnchorType Anchor;
    public GameObject SpawnInitializator;
    public string SpawnInitializatorTriggerLabel;

    public override void Cancel()
    {}

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        GameObject instance=Instantiate(Prefab, new Vector3(0, 0, 0), Quaternion.identity);
        if (Parent != null)
        {
            instance.transform.parent = Parent.transform;
        }
        if (Bounds!=null)
        {
            Vector2 topLeft = Bounds.GetTopLeft();
            Vector2 bottomRight = Bounds.GetBottomRight();
            Vector3 newPosition = new Vector3(
                Random.Range(topLeft.x, bottomRight.x),
                Random.Range(topLeft.y, bottomRight.y),
                instance.transform.position.z
            );
            switch (Anchor)
            {
                case RectBounds.AnchorType.Bottom:
                    newPosition.y = Bounds.GetBottom();
                    break;
                case RectBounds.AnchorType.Top:
                    newPosition.y = Bounds.GetTop();
                    break;
                case RectBounds.AnchorType.Left:
                    newPosition.x = Bounds.GetLeft();
                    break;
                case RectBounds.AnchorType.Right:
                    newPosition.x = Bounds.GetRight();
                    break;
            }
            instance.transform.position = newPosition;
        }
        else
        {
            instance.transform.localPosition = Vector3.zero;
        }
        if (SpawnInitializator != null)
        {
            SpawnInitializator.GetComponent<TargetHolder>().Target=instance;
            TriggerInterface[] Triggers;
            Triggers = SpawnInitializator.GetComponents<TriggerInterface>();
            foreach (TriggerInterface trigger in Triggers)
            {
                if (SpawnInitializatorTriggerLabel.Equals(trigger.GetLabel()))
                {
                    trigger.Trigger();
                }
            }
        }
    }

}
