﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeRandomRangePositionTrigger : TriggerInterface
{

    public RectBounds Bounds;
    public RectBounds.AnchorType Anchor;
    public GameObject Object;
    public bool Animate;
    public float MinTime=1;
    public float MaxTime=2;
    public AnimationCurve AnimCurveX;
    public AnimationCurve AnimCurveY;
    public TriggerInterface TriggerOnArrive;
    public bool Local;

    private bool IsAnimating = false;
    private Vector3 StartingPos;
    private Vector3 TargetPos;
    private float TotalTime;
    private float CurrentTimer = 0;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        Vector2 topLeft = Bounds.GetTopLeft();
        Vector2 bottomRight = Bounds.GetBottomRight();
        TargetPos = new Vector3(
            Random.Range(topLeft.x, bottomRight.x),
            Random.Range(topLeft.y, bottomRight.y),
            Object.transform.position.z
        );
        switch (Anchor)
        {
            case RectBounds.AnchorType.Bottom:
                TargetPos.y = Bounds.GetBottom();
                break;
            case RectBounds.AnchorType.Top:
                TargetPos.y = Bounds.GetTop();
                break;
            case RectBounds.AnchorType.Left:
                TargetPos.x = Bounds.GetLeft();
                break;
            case RectBounds.AnchorType.Right:
                TargetPos.x = Bounds.GetRight();
                break;
        }
        if (Animate)
        {
            StartingPos = (Local)? Object.transform.localPosition:Object.transform.position;
            TotalTime = Random.Range(MinTime, MaxTime);
            CurrentTimer = 0;
            IsAnimating = true;
        }
        else
        {
            if (Local)
            {
                Object.transform.localPosition = TargetPos;
            }
            else
            {
                Object.transform.position = TargetPos;
            }
        }
    }

    protected virtual void Update()
    {
        if (!IsAnimating)
        {
            return;
        }
        float t = CurrentTimer / TotalTime;
        Vector3 newPos= new Vector3(
            StartingPos.x + ((TargetPos.x - StartingPos.x) * AnimCurveY.Evaluate(t)),
            StartingPos.y + ((TargetPos.y - StartingPos.y) * AnimCurveY.Evaluate(t)),
            Object.transform.position.z
        );
        CurrentTimer += Time.deltaTime;
        if (CurrentTimer >= TotalTime)
        {
            newPos.x = TargetPos.x;
            newPos.y = TargetPos.y;
            IsAnimating = false;
            if (TriggerOnArrive != null)
            {
                TriggerOnArrive.Trigger();
            }
        }
        if (Local)
        {
            Object.transform.localPosition = newPos;
        }
        else
        {
            Object.transform.position = newPos;
        }
    }

}
