﻿using System;
using UnityEngine;

public class ChangePositionTrigger: TriggerInterface
{

    public Transform NewPosition;
    public GameObject Object;
    public bool Animate;
    public float MinTime = 1;
    public float MaxTime = 2;
    public AnimationCurve AnimCurveX;
    public AnimationCurve AnimCurveY;
    public TriggerInterface TriggerOnArrive;
    public bool Local;

    private bool IsAnimating = false;
    private Vector3 StartingPos;
    private Vector3 TargetPos;
    private float TotalTime;
    private float CurrentTimer = 0;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        TargetPos = (Local) ? NewPosition.localPosition : NewPosition.position;
        if (Animate)
        {
            StartingPos = (Local) ? Object.transform.localPosition : Object.transform.position;
            TotalTime = UnityEngine.Random.Range(MinTime, MaxTime);
            CurrentTimer = 0;
            IsAnimating = true;
        }
        else
        {
            if (Local)
            {
                Object.transform.localPosition = TargetPos;
            }
            else
            {
                Object.transform.position = TargetPos;
            }
        }
    }

    protected virtual void Update()
    {
        if (!IsAnimating)
        {
            return;
        }
        float t = CurrentTimer / TotalTime;
        Vector3 newPos = new Vector3(
            StartingPos.x + ((TargetPos.x - StartingPos.x) * AnimCurveY.Evaluate(t)),
            StartingPos.y + ((TargetPos.y - StartingPos.y) * AnimCurveY.Evaluate(t)),
            Object.transform.position.z
        );
        CurrentTimer += Time.deltaTime;
        if (CurrentTimer >= TotalTime)
        {
            newPos.x = TargetPos.x;
            newPos.y = TargetPos.y;
            IsAnimating = false;
            if (TriggerOnArrive != null)
            {
                TriggerOnArrive.Trigger();
            }
        }
        if (Local)
        {
            Object.transform.localPosition = newPos;
        }
        else
        {
            Object.transform.position = newPos;
        }
    }

}
