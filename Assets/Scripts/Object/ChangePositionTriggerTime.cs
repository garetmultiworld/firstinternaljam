﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePositionTriggerTime : TriggerInterface
{

    public ChangePositionTrigger changePositionTrigger;
    public GameObject MinDataHolder;
    public string MinDataLabel;
    public GameObject MaxDataHolder;
    public string MaxDataLabel;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (changePositionTrigger == null)
        {
            return;
        }
        DataHolder[] dataHolders;
        if (MinDataHolder != null)
        {
            dataHolders = MinDataHolder.GetComponents<DataHolder>();
        }
        else
        {
            dataHolders = this.gameObject.GetComponents<DataHolder>();
        }
        foreach (DataHolder data in dataHolders)
        {
            if (MinDataLabel.Equals(data.Name))
            {
                changePositionTrigger.MinTime = data.FloatValue;
                break;
            }
        }
        if (MaxDataHolder != null)
        {
            dataHolders = MaxDataHolder.GetComponents<DataHolder>();
        }
        else
        {
            dataHolders = this.gameObject.GetComponents<DataHolder>();
        }
        foreach (DataHolder data in dataHolders)
        {
            if (MaxDataLabel.Equals(data.Name))
            {
                changePositionTrigger.MaxTime = data.FloatValue;
                break;
            }
        }
    }
}
