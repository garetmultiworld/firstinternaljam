﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableObjectTrigger : TriggerInterface
{

    public GameObject TheObject;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        TheObject.SetActive(true);
    }
}
