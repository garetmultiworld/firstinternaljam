﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDebugData : TriggerInterface
{

    public GameObject Holder;
    public string LabelData;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        DataHolder[] dataHolders;
        if (Holder != null)
        {
            dataHolders = Holder.GetComponents<DataHolder>();
        }
        else
        {
            dataHolders = this.gameObject.GetComponents<DataHolder>();
        }
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    default://assumes string
                        Debug.Log(data.Value);
                        break;
                    case DataHolder.Type.Number:
                        Debug.Log(data.FloatValue);
                        break;
                    case DataHolder.Type.Integer:
                        Debug.Log(data.IntValue);
                        break;
                }
                break;
            }
        }
    }
}
