﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDebugCheck : TriggerInterface
{

    public string Text;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        Debug.Log(Text);
    }

}
