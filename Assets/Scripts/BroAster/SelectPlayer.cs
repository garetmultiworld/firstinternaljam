﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectPlayer : TriggerInterface
{

    protected static int SelectedPlayers = 0;

    public DataHolder SelectedPlayer;
    public GameObject[] Players;
    public Animator[] Animators;
    public Animator[] Specials;
    public AnimationParameterTrigger SpecialTrigger;
    public AnimationParameterTrigger AnimatorUp;
    public AnimationParameterTrigger AnimatorLeft;
    public AnimationParameterTrigger AnimatorRight;
    public AnimationParameterTrigger AnimatorDown;

    public TriggerInterface TriggerWhenBothSelected;

    protected GameObject CurrentPlayer;
    protected bool selected;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        int selected = SelectedPlayer.IntValue;
        foreach(GameObject player in Players)
        {
            player.SetActive(false);
        }
        CurrentPlayer = Players[selected];
        CurrentPlayer.SetActive(true);
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        SpecialTrigger.Animator = Specials[selected];
        AnimatorUp.Animator = Animators[selected];
        AnimatorLeft.Animator = Animators[selected];
        AnimatorRight.Animator = Animators[selected];
        AnimatorDown.Animator = Animators[selected];
    }

    public void SetCharacter(int index)
    {
        SelectedPlayer.SetIntValue(index);
        if (!selected)
        {
            selected = true;
            SelectedPlayers++;
            if (SelectedPlayers==2&&TriggerWhenBothSelected != null)
            {
                TriggerWhenBothSelected.Trigger();
            }
        }
    }

    public void ReloadEverything()
    {
        SelectedPlayers = 0;
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        Resources.UnloadUnusedAssets();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }

}
