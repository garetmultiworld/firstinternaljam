﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeRigidbody2DBodyTypeTrigger : TriggerInterface
{

    public RigidbodyType2D NewType;
    public Rigidbody2D TheRigidBody2D;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        TheRigidBody2D.bodyType = NewType;
    }

}
