﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicChannelControlTrigger : TriggerInterface
{

    public string Channel;
    public MusicPlayer.controlTypes Type;
    public float TransitionTime = 1;
    public float Volume=1;

    private bool IsAnimating = false;

    private float CurrentVolume;
    private float StartingVolume;
    private float Speed;

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        switch (Type)
        {
            case MusicPlayer.controlTypes.Stop:
                AudioManager.Instance.StopMusic(Channel);
                break;
            case MusicPlayer.controlTypes.Pause:
                AudioManager.Instance.PauseMusic(Channel);
                break;
            case MusicPlayer.controlTypes.Resume:
                AudioManager.Instance.ResumeMusic(Channel);
                break;
            case MusicPlayer.controlTypes.StopWithFade:
                AudioManager.Instance.StopWithFadeMusic(Channel, TransitionTime);
                break;
            case MusicPlayer.controlTypes.Volume:
                AudioManager.Instance.SetMusicChannelVolume(Channel, Volume);
                break;
            case MusicPlayer.controlTypes.GradualVolume:
                StartingVolume = AudioManager.Instance.GetMusicChannelVolume(Channel);
                if (StartingVolume > Volume)
                {
                    Speed = (CurrentVolume - Volume) / TransitionTime;
                }
                else
                {
                    Speed = (Volume - CurrentVolume) / TransitionTime;
                }
                IsAnimating = true;
                break;
        }
    }

    public override void Cancel()
    {
        IsAnimating = false;
    }

    protected virtual void Update()
    {
        if (!IsAnimating)
        {
            return;
        }
        CurrentVolume = AudioManager.Instance.GetMusicChannelVolume(Channel);
        if (
            (CurrentVolume - 0.01f) <= Volume &&
            (CurrentVolume + 0.01f) >= Volume
        )
        {
            AudioManager.Instance.SetMusicChannelVolume(Channel, Volume);
            IsAnimating = false;
        }
        else
        {
            if (Volume > StartingVolume)
            {
                CurrentVolume += Speed * Time.deltaTime;
            }
            else
            {
                CurrentVolume -= Speed * Time.deltaTime;
            }
            AudioManager.Instance.SetMusicChannelVolume(Channel, CurrentVolume);
        }
    }

}
