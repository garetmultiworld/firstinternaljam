﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    #region Static Instance
    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<AudioManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned AudioManager", typeof(AudioManager)).GetComponent<AudioManager>();
                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    #endregion

    #region Fields
    [SerializeField]
    [Range(0f, 1f)]
    protected float SfxVolume = 1f;
    [SerializeField]
    protected bool SfxOn = true;
    public Sound[] soundEffects;
    [SerializeField]
    [Range(0f, 1f)]
    protected float MusicVolume = 1f;
    [SerializeField]
    protected bool MusicOn = true;
    public AudioChannel[] MusicChannels;

    protected Dictionary<string, Sound> effectsByName=new Dictionary<string, Sound>();
    protected Dictionary<string, Sound> musicByName = new Dictionary<string, Sound>();
    protected Dictionary<string, AudioChannel> channelsByMusicName = new Dictionary<string, AudioChannel>();
    protected Dictionary<string, AudioChannel> channelsByName = new Dictionary<string, AudioChannel>();
    #endregion

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        foreach(AudioChannel audioChannel in MusicChannels)
        {
            audioChannel.musicPlayer = gameObject.AddComponent<MusicPlayer>();
            audioChannel.musicPlayer.volume = audioChannel.volume;
            channelsByName.Add(audioChannel.name, audioChannel);
            foreach (Sound sound in audioChannel.sounds)
            {
                musicByName.Add(sound.name, sound);
                channelsByMusicName.Add(sound.name, audioChannel);
            }
        }
        foreach (Sound sound in soundEffects)
        {
            effectsByName.Add(sound.name, sound);
        }
    }

    public void ChangeMusicChannel(string musicName, string destinationChannel)
    {
        AudioChannel destination= channelsByName[destinationChannel];
        channelsByMusicName.Remove(musicName);
        channelsByMusicName.Add(musicName, destination);
    }

    public void StopLoopingSound(AudioSource source)
    {
        if (source != null)
        {
            Destroy(source.gameObject);
        }
    }

    public void SetSfxVolume(float volume)
    {
        SfxVolume = volume;
        foreach (Sound sound in soundEffects)
        {
            sound.MultiplyActiveVolume(SfxVolume);
        }
    }

    public void SetSfxOn(bool status)
    {
        SfxOn = status;
        foreach (Sound sound in soundEffects)
        {
            sound.SetMute(SfxOn);
        }
    }

    public float GetMusicChannelVolume(string channel)
    {
        return channelsByName[channel].musicPlayer.volume;
    }

    public void SetMusicChannelVolume(string channel, float volume)
    {
        channelsByName[channel].musicPlayer.SetVolume(volume);
    }

    public void SetMusicVolume(float volume)
    {
        MusicVolume = volume;
        foreach (AudioChannel audioChannel in MusicChannels)
        {
            audioChannel.musicPlayer.SetVolumeMultiplier(MusicVolume);
        }
    }

    public void SetMusicOn(bool status)
    {
        MusicOn = status;
        foreach (AudioChannel audioChannel in MusicChannels)
        {
            audioChannel.musicPlayer.SetMute(MusicOn);
        }
    }

    public void StopMusic(string channel)
    {
        channelsByName[channel].musicPlayer.Stop();
    }

    public void StopWithFadeMusic(string channel,float transitionTime)
    {
        channelsByName[channel].musicPlayer.StopWithFade(transitionTime);
    }

    public void PauseMusic(string channel)
    {
        channelsByName[channel].musicPlayer.Pause();
    }

    public void ResumeMusic(string channel)
    {
        channelsByName[channel].musicPlayer.Resume();
    }

    public void PlayMusic(string music,float from)
    {
        AudioChannel audioChannel = channelsByMusicName[music];
        Sound sound = musicByName[music];
        audioChannel.musicPlayer.PlayMusic(sound, MusicVolume, from);
    }

    public void PlayMusicWithFade(string music, float transitionTime, float waitTime , float from)
    {
        AudioChannel audioChannel = channelsByMusicName[music];
        Sound sound = musicByName[music];
        audioChannel.musicPlayer.PlayMusicWithFade(sound, MusicVolume, transitionTime, waitTime, from);
    }

    public void PlayMusicWithCrossFade(string music, float transitionTime , float from)
    {
        AudioChannel audioChannel = channelsByMusicName[music];
        Sound sound = musicByName[music];
        audioChannel.musicPlayer.PlayMusicWithCrossFade(sound, MusicVolume, transitionTime, from);
    }

    public AudioSource PlaySfx(string sfx,GameObject source)
    {
        Sound sound = effectsByName[sfx];
        return PlaySfx(
            sound.clip,
            source.transform,
            sound.pitch,
            sound.pan,
            sound.volume * SfxVolume,
            sound.reverb,
            sound.loop,
            sound.audioMixerGroup,
            sound
        );
    }

    protected AudioSource PlaySfx(AudioClip sfx, Transform source,
        float pitch, float pan, float volume , float reverb , bool loop,
        AudioMixerGroup audioGroup, Sound sound)
    {
        if (!SfxOn)
        {
            return null;
        }

        AudioSource audioSource;
        GameObject temporaryAudioHost = null;

        // we create a temporary game object to host our audio source
        temporaryAudioHost = new GameObject("TempAudio");
        // we add an audio source to that host
        var newAudioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource;
        audioSource = newAudioSource;
        // we set the temp audio's position
        audioSource.transform.parent = source;
        audioSource.transform.localPosition= Vector3.zero;

        audioSource.time = 0.0f; // Reset time in case it's a reusable one.

        // we set that audio source clip to the one in paramaters
        audioSource.clip = sfx;

        audioSource.pitch = pitch;
        audioSource.reverbZoneMix = reverb;
        audioSource.panStereo = pan;

        // we set the audio source volume to the one in parameters
        audioSource.volume = volume;
        // we set our loop setting
        audioSource.loop = loop;
        // Assign an audio mixer group.
        if (audioGroup)
            audioSource.outputAudioMixerGroup = audioGroup;


        // we start playing the sound
        audioSource.Play();

        if (!loop)
        {
            // we destroy the host after the clip has played (if it not tag for reusability.
            Destroy(temporaryAudioHost, sfx.length);
            return null;
        }
        else
        {
            sound.ActiveSounds.Add(audioSource);
            return audioSource;
        }

    }

}
