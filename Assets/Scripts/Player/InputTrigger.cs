﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTrigger : MonoBehaviour
{

    public string InputName;
    public TriggerInterface Trigger;
    public float Cooldown = 0.1f;

    protected bool Ready=true;
    protected float CooldownTimer=0f;

    void Update()
    {
        if (!Ready)
        {
            CooldownTimer += Time.deltaTime;
            if (CooldownTimer >= Cooldown)
            {
                Ready = true;
            }
        }
        if (Input.GetAxis(InputName)!=0&&Ready)
        {
            if (Trigger != null)
            {
                Trigger.Trigger();
            }
            CooldownTimer = 0f;
            Ready = false;
        }
    }

    public void SetReady(bool ready)
    {
        enabled = ready;
        Ready = ready;
    }
}
