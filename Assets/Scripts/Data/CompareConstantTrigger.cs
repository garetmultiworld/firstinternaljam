﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompareConstantTrigger : CompareBase
{

    public GameObject Holder;
    public string LabelData;
    public bool DataFromTarget;
    public bool DataOnLeft=true;
    public Operation operation;
    public DataHolder.Type ConstantType;
    public string Value;
    public float NumberValue;
    public int IntegerValue;

    public TriggerInterface IfTrue;
    public TriggerInterface IfFalse;

    protected DataHolder TempHolder;

    void Awake()
    {
        TempHolder = gameObject.AddComponent<DataHolder>();
    }

    public override void StartComparison()
    {
        DataHolder DynamicData;
        TempHolder.type = ConstantType;
        TempHolder.SetValue(Value);
        TempHolder.SetFloatValue(NumberValue);
        TempHolder.SetIntValue(IntegerValue);
        if (DataFromTarget)
        {
            DynamicData = GetData(Holder.GetComponent<TargetHolder>().Target, LabelData);
        }
        else
        {
            DynamicData = GetData(Holder, LabelData);
        }
        if (DynamicData == null)
        {
            return;
        }
        if (DataOnLeft)
        {
            Compare(DynamicData, TempHolder, operation, IfTrue, IfFalse);
        }
        else
        {
            Compare(TempHolder, DynamicData, operation, IfTrue, IfFalse);
        }
    }

}
