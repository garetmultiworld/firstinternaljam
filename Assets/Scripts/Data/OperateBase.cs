﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperateBase : DataHolder
{
    public enum Operation
    {
        Sum,
        Substract,
        Multiply,
        Divide
    }

    protected DataHolder GetData(GameObject parent, string label)
    {
        DataHolder result = null;
        DataHolder[] dataHolders;
        if (parent != null)
        {
            dataHolders = parent.GetComponents<DataHolder>();
        }
        else
        {
            dataHolders = this.gameObject.GetComponents<DataHolder>();
        }
        foreach (DataHolder data in dataHolders)
        {
            if (label.Equals(data.Name))
            {
                result = data;
                break;
            }
        }
        return result;
    }

    public void Operate(DataHolder a, DataHolder b, Operation operation)
    {
        switch (operation)
        {
            case Operation.Sum:
                Sum(a, b);
                break;
            case Operation.Substract:
                Substract(a, b);
                break;
            case Operation.Multiply:
                Multiply(a, b);
                break;
            case Operation.Divide:
                Divide(a, b);
                break;
        }
    }

    public void Sum(DataHolder a, DataHolder b)
    {
        switch (a.type)
        {
            case DataHolder.Type.Number:
                SetFloatValue(a.FloatValue + b.FloatValue);
                break;
            case DataHolder.Type.Integer:
                SetIntValue(a.IntValue + b.IntValue);
                break;
        }
    }

    public void Substract(DataHolder a, DataHolder b)
    {
        switch (a.type)
        {
            case DataHolder.Type.Number:
                SetFloatValue(a.FloatValue - b.FloatValue);
                break;
            case DataHolder.Type.Integer:
                SetIntValue(a.IntValue - b.IntValue);
                break;
        }
    }

    public void Multiply(DataHolder a, DataHolder b)
    {
        switch (a.type)
        {
            case DataHolder.Type.Number:
                SetFloatValue(a.FloatValue * b.FloatValue);
                break;
            case DataHolder.Type.Integer:
                SetIntValue(a.IntValue * b.IntValue);
                break;
        }
    }

    public void Divide(DataHolder a, DataHolder b)
    {
        switch (a.type)
        {
            case DataHolder.Type.Number:
                SetFloatValue(a.FloatValue / b.FloatValue);
                break;
            case DataHolder.Type.Integer:
                SetIntValue(a.IntValue / b.IntValue);
                break;
        }
    }
}
