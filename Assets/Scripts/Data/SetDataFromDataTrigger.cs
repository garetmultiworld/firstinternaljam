﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetDataFromDataTrigger : TriggerInterface
{

    public GameObject Holder;
    public bool FromTarget;
    public string LabelData;
    public GameObject HolderDestination;
    public bool FromTargetDestination;
    public string LabelDataDestination;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        DataHolder Destination=null;
        DataHolder[] dataHolders;
        GameObject tempHolder;
        if (HolderDestination != null)
        {
            tempHolder = HolderDestination;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTargetDestination)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelDataDestination.Equals(data.Name))
            {
                Destination = data;
                break;
            }
        }
        if (Destination == null)
        {
            return;
        }

        if (Holder != null)
        {
            tempHolder = Holder;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    default://assumes string
                        Destination.SetValue(data.Value);
                        break;
                    case DataHolder.Type.Number:
                        Destination.SetFloatValue(data.FloatValue);
                        break;
                    case DataHolder.Type.Integer:
                        Destination.SetIntValue(data.IntValue);
                        break;
                }
                break;
            }
        }
    }

}
