﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataHolder : MonoBehaviour
{

    public enum Type
    {
        Text,
        Number,
        Integer
    }

    public string Name;
    public Type type;

    public TriggerInterface OnChangeTrigger;

    protected string InternalValue;
    protected float InternalFloatValue;
    protected int InternalIntValue;

    public virtual string Value
    {
        get { 
            return InternalValue; 
        }
    }

    public virtual float FloatValue
    {
        get
        {
            return InternalFloatValue;
        }
    }

    public virtual int IntValue
    {
        get
        {
            return InternalIntValue;
        }
    }

    public void SetIntValue(int newValue)
    {
        InternalIntValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Trigger();
        }
    }

    public void SetFloatValue(float newValue)
    {
        InternalFloatValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Trigger();
        }
    }

    public void SetValue(string newValue)
    {
        InternalValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Trigger();
        }
    }

}
