﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetDataTrigger : TriggerInterface
{
    public GameObject Holder;
    public bool FromTarget;
    public string LabelData;
    public string Value;
    public float NumberValue;
    public int IntegerValue;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        DataHolder[] dataHolders;
        GameObject tempHolder;
        if (Holder != null)
        {
            tempHolder = Holder;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    default://assumes string
                        data.SetValue(Value);
                        break;
                    case DataHolder.Type.Number:
                        data.SetFloatValue(NumberValue);
                        break;
                    case DataHolder.Type.Integer:
                        data.SetIntValue(IntegerValue);
                        break;
                }
                break;
            }
        }
    }
}
