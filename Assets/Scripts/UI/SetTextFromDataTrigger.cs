﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetTextFromDataTrigger : TriggerInterface
{

    public string Prefix;
    public GameObject Holder;
    public bool FromTarget;
    public string LabelData;
    public string Sufix;
    public Text text;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        DataHolder[] dataHolders;
        GameObject tempHolder;
        if (Holder != null)
        {
            tempHolder = Holder;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    case DataHolder.Type.Number:
                        text.text = Prefix + data.FloatValue.ToString() + Sufix;
                        break;
                    case DataHolder.Type.Integer:
                        text.text = Prefix + data.IntValue.ToString() + Sufix;
                        break;
                }
                break;
            }
        }
    }

}
