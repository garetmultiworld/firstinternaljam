﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogType
{
    public enum LocationType
    {
        Player,
        Object,
        Random
    }

    public string Name;
    public LocationType locationType;
    public string ParentName;
    public Vector3 RelativePosition;
}
