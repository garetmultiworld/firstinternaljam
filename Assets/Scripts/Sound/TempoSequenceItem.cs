﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TempoSequenceItem
{

    public bool IsSilence = false;
    public TempoSequenceItemNote[] Notes;

}
