﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicInfo : MonoBehaviour
{

    public float Tempo;
    public GameObject TempoDataHolder;
    public string LabelTempoData;
    public TempoSequence tempoSequence;
    public bool Loop;

    void Awake()
    {
        DataHolder[] dataHolders;
        if (TempoDataHolder != null)
        {
            dataHolders = TempoDataHolder.GetComponents<DataHolder>();
        }
        else
        {
            dataHolders = this.gameObject.GetComponents<DataHolder>();
        }
        foreach (DataHolder data in dataHolders)
        {
            if (LabelTempoData.Equals(data.Name))
            {
                data.type = DataHolder.Type.Number;
                data.SetFloatValue(Tempo);
                break;
            }
        }
    }

}
