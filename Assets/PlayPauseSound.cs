﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPauseSound : MonoBehaviour
{
    public void onClick()
    {
        AkSoundEngine.PostEvent("Pausa", gameObject);
    }
}
