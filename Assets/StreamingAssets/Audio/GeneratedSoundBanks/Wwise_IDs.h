/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BABYULTI = 880530411U;
        static const AkUniqueID CORRECTP1 = 3138935408U;
        static const AkUniqueID CORRECTP2 = 3138935411U;
        static const AkUniqueID CRONOMENTRO = 607180681U;
        static const AkUniqueID FREIDORA = 2676847199U;
        static const AkUniqueID GAMEOVER = 4158285989U;
        static const AkUniqueID MUSIC_TRACK = 757172466U;
        static const AkUniqueID MUSICSTOP = 3978196478U;
        static const AkUniqueID PAUSA = 3092587489U;
        static const AkUniqueID POLLAULTI = 3208893193U;
        static const AkUniqueID POLLO_EN_FREIDORA = 1794756222U;
        static const AkUniqueID QUAK = 4065015535U;
        static const AkUniqueID ROCKULTI = 3016216400U;
        static const AkUniqueID START = 1281810935U;
        static const AkUniqueID WRONG = 366552496U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID ULTIATTACK = 1890768391U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID QUAK = 4065015535U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSICBUSS = 2936792103U;
        static const AkUniqueID ULTIBUS = 2822252859U;
        static const AkUniqueID WRONG_INPUT = 2050680508U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
